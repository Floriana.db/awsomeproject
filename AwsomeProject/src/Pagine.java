package src;


public enum Pagine {
	HOME,
	ACCEDI,
	GESTIONE_CARRELLO,
	RICERCA_PRODOTTO,
	STATO_ORDINE,
	ESCI;
}
